<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MySubObjectRepository")
 */
class MySubObject
{
    const STATUS_WAITING = 'En cours';
    const STATUS_PROCESSED = 'Réalisé';
    const STATUS_ERROR = 'Échec';

    public static $statuses = array(
        self::STATUS_WAITING,
        self::STATUS_PROCESSED,
        self::STATUS_ERROR,
    );
    public static $statusChoices = array(
        self::STATUS_WAITING => self::STATUS_WAITING,
        self::STATUS_PROCESSED => self::STATUS_PROCESSED
    );
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Nom de l'objet ne peut être vide")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Nom de l'objet ne peut être vide")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\MyObject", mappedBy="subObjects")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $objects;
    
    public function __construct($status = self::STATUS_WAITING)
    {
        $this->setStatus($status);
        $this->objects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        if($this->getName()) {
            return $this->getName();
        } else {
            return 'no name';
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add object
     *
     * @param \App\Entity\MyObject $object
     *
     * @return MySubObject
     */
    public function addObject(MyObject $object)
    {
        $this->objects[] = $object;

        return $this;
    }

    /**
     * Remove object
     *
     * @param \App\Entity\MyObject $object
     */
    public function removeObject(MyObject $object)
    {
        $this->objects->removeElement($object);
    }

    /**
     * Get objects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjects()
    {
        return $this->objects;
    }
}
