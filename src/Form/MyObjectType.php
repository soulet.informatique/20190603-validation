<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class MyObjectType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        
        $file = $builder->getData(); 
        
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nom de l\'objet',
                'required'      => false,
//                'required' => true
            ))
            ->add('number', NumberType::class, array(
                'required'      => false,
                'label' => 'Numéro de l\'objet',
            ))
        ;
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\MyObject',
        ));
    }

}
