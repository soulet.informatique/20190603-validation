<?php

namespace App\Controller;

use App\Entity\MyObject;
use App\Form\MyObjectType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * MyObject controller.
 * @Route("/my-object")
 */
class MyObjectController extends AbstractController
{
    /**
     * @Route("/", name="objects")
     */
    public function indexAction(Request $request)
    {
        $objects = $this->getDoctrine()->getRepository('App\Entity\MyObject')->queryObjects()->getQuery()->getResult();
        return $this->render('MyObject\index.html.twig', array(
            'objects' => $objects,
        ));

    }

    /**
     * @Route("/{id}/view", name="my_object_view")
     * @ParamConverter("myObject", class="App:MyObject", options={"mapping": {"id": "id"}})
     */
    public function viewAction(Request $request, MyObject $myObject)
    {
        return $this->render('MyObject\view.html.twig', array(
            'myObject' => $myObject
        ));
    }

    /**
     *  @Route("/{id}/update", name="my_object_edit")
     *  @ParamConverter("myObject", class="App:MyObject", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, MyObject $myObject)
    {
        $form = $this->createForm(MyObjectType::class, $myObject, array(
            'action' => $this->generateUrl('my_object_edit', array('id' => $myObject->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Modifier'));
        $form->handleRequest($request);
        if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('my_object_view', array('id' => $myObject->getId()));
        }

        return $this->render('App:MyObject:edit.html.twig', array(
            'myObject' => $myObject,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/add", name="my_object_add")
     */
    public function addAction(Request $request)
    {
        $myObject = new MyObject();

        $form = $this->createForm(MyObjectType::class, $myObject, array(
            'action' => $this->generateUrl('my_object_add'),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Ajouter'));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($myObject);
            $em->flush();

            return $this->redirectToRoute('my_object_view', array('id' => $myObject->getId()));
        }
        return $this->render('MyObject\edit.html.twig', array(
            'myObject' => null,
            'form' => $form->createView(),
        ));
    }
}
